package com.indeed.suggest;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import com.indeed.suggest.prefixTree.Trie;

public class Suggester implements LogEntryProcessor {
    private Trie trie; 
    public Suggester() {
        trie = new Trie();
    }

    /*
      @param logEntryMap - Map containing keys and values in each log line
      This is a call back method that LogReader.read() calls on every line in the input file, after parsing them into key-value pairs
    */
    public void processEntries(Map<String, String> logEntryMap) {
        trie.insert(logEntryMap.get("match"));
    }

    /**
     * Returns a list of suggestions for a given user query.
     * @param query the string that the user has typed so far
     * @param k the maximum number of suggestions requested
     */
    public List<String> getTopSuggestions(String query, int k) {
        //return Arrays.asList("Austin, TX", "Palo Alto, CA", "Boston, MA", "Miami, FL");
        return trie.autocomplete(query, k);
    }

    // main() for command-line testing
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Need to provide log file (query.log) as argument");
        }
        final String inputFile = args[0];
        Suggester suggester = new Suggester();
        LogReader logReader = new LogReader(inputFile, suggester);

        long elapsedTime = -System.currentTimeMillis();
        logReader.read();
        elapsedTime += System.currentTimeMillis();

        System.out.println(elapsedTime + "ms to read file");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Type 'quit' or 'exit' when you're done.");
            while (true) {
                System.out.print("query> ");
                String line = in.readLine();
                if ("".equals(line)) continue;
                if (line == null || "quit".equals(line) || "exit".equals(line)) break;

                elapsedTime = -System.currentTimeMillis();
                List<String> suggestions = suggester.getTopSuggestions(line, 10);
                elapsedTime += System.currentTimeMillis();

                System.out.println("Suggestions for '" + line + "' " + suggestions + " fetched in " + elapsedTime + "ms");
                System.out.println("Result count: "+ suggestions.size());
            }
            System.out.println();
        } finally {
            in.close();
        }
    }
}
