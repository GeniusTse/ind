package com.indeed.suggest.prefixTree;

import java.util.List;
import java.util.ArrayList;

public class Trie {
    private TrieNode root;
 
    public Trie() {
        root = new TrieNode(' '); 
    }
 
    /**
     * Insert given word to Trie for autocomplete
     * @param word
     */
    public void insert(String word) {
        if (isWordExist(word)) 
            return;    
        
        TrieNode current = root; 
        TrieNode previous ;
        for (char c : word.toCharArray()) {
        	previous = current;
            TrieNode child = current.getChild(c);
            if (child != null) {
                current = child;
                child.parent = previous;
            } else {
                current.addChild(c);
                current = current.getChild(c);
                current.parent = previous;
            }
        }
        current.isEnd = true;
    }
    
    /**
     * Check is given word exist in the Trie
     * @param word
     * @return true if exist 
     */
    public boolean isWordExist(String word) {
        TrieNode current = root;      
        for (char c : word.toCharArray()) {
            if (current.getChild(c) == null)
                return false;
            else {
                current = current.getChild(c);
            }
        }      
        if (current.isEnd == true) {       
            return true;
        }
        return false;
    }
    
    /**
     * Perform autocompelte on Trie tree. It is expect to return a list of words which prefix matched with given query
     * @param query query for search and do autocomplete
     * @param limit the maximum number of result 
     * @return words which prefix matched with query
     */
    public List autocomplete(String query, int limit) {     
        TrieNode lastNode = root;
        if (query.length() == 0)
            return new ArrayList();

        for (int i = 0; i< query.length(); i++) {
            lastNode = lastNode.getChild(query.charAt(i));	     
            if (lastNode == null) 
                return new ArrayList();      
        }
        
        List result = lastNode.getWords(limit);
        return (result.size() <= limit) ?result :result.subList(0, limit);
    }
}  