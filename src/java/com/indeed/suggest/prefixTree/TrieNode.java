package com.indeed.suggest.prefixTree;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

public class TrieNode {
    char data;
    LinkedList<TrieNode> children; // use LinkedList to store node children so that to maintain the sequence.
    TrieNode parent;
    boolean isEnd;
 
    public TrieNode(char c) {
    	data = c;
        children = new LinkedList<TrieNode>();
        isEnd = false;
    }  

    /**
     * Add child TriNode to current if it is not exist in duplicate map
     * @param c character to be added
     */
    public void addChild(char c){
        children.add(new TrieNode(c));
    }
    
    /**
     * Get child TriNode if data matched ignore case with given c 
     * @param c character to find
     */
    public TrieNode getChild(char c) {
        if (children == null)
            return null;
        
        for (TrieNode eachChild : children)
            if (Character.toLowerCase(eachChild.data) == Character.toLowerCase(c))
                return eachChild;
        return null;
    }
    
    /**
     * Composite list for words from current TriNode with its parent and all child paths
     * @param limit
     * @return List of String
     */
    public List<String> getWords(int limit) {
        List<String> list = new ArrayList<String>();
        if (isEnd)
           list.add(compositeWord());
               
        if (children == null) 
            return list;

        // loop children and form words 
        for (int i=0; i< children.size(); i++) {
            if (children.get(i) != null) {
                if (list.size() < limit) // stop early
                    list.addAll(children.get(i).getWords(limit));
            }
        }
        return list; 
    }
    
    /**
     * Compiste a word from current to its parent node and current node data
     * @return
     */
	public String compositeWord() {
		if (parent == null) {
		     return "";
		} else {
		     return parent.compositeWord() + new String(new char[] {data});
		}
    }
    
    public TrieNode getParent() {
        return parent;
    }
    public char getData() {
        return data;
    }
    public List getChildren() {
        return children;
    }
    public boolean isEnd() {
        return isEnd;
    }

}