package com.indeed.suggest.prefixTree.test;

import org.junit.Test;
import static org.junit.Assert.*;
import com.indeed.suggest.prefixTree.Trie;
import com.indeed.suggest.prefixTree.TrieNode;
import java.util.*;

public class TestTrie {

    @Test
    public void testInsertSingleWord() {
        Trie trie = new Trie(); 
        String word = "San Diego";
        trie.insert(word);
        assertTrue(trie.isWordExist(word));
        assertFalse(trie.isWordExist("San"));
    }

    // test two word without overlap
    @Test
    public void testInsertTwoWordWithoutOverlap() {
        Trie trie = new Trie(); 
        String word1 = "San Diego";
        String word2 = "California";
        trie.insert(word1);
        trie.insert(word2);
        assertTrue(trie.isWordExist(word1));
        assertTrue(trie.isWordExist(word2));
    }

    // test two word with overlap
    @Test
    public void testInsertTwoWordWithOverlap() {
        Trie trie = new Trie(); 
        String word1 = "San Diego";
        String word2 = "San Francisco";
        trie.insert(word1);
        trie.insert(word2);
        assertTrue(trie.isWordExist(word1));
        assertTrue(trie.isWordExist(word2));
    }

    // test no word 
    @Test
    public void testInsertTwoWordWithNoWord() {
        Trie trie = new Trie(); 
        String word1 = "San Diego";
        assertFalse(trie.isWordExist(word1));
    }

    // test autocomplete with no word
    @Test
    public void testAutoCompleteWithNoWord() {
        Trie trie = new Trie(); 
        List result = trie.autocomplete("San", 10);
        assertTrue("Return empty list", result.size() == 0);
    }

    // test 2 of 3 matched
    @Test
    public void testAutoComplete2Of3Matched() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        List result = trie.autocomplete("San", 10);
        assertTrue("Return empty list", result.size() == 2);
        assertTrue("1st result is San Diego", result.get(0).equals("San Diego"));
        assertTrue("2nd result is San Diego", result.get(1).equals("San Jose"));
    }

    // test 1 of 3 matched
    @Test
    public void testAutoComplete1Of3Matched() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        List result = trie.autocomplete("San J", 10);
        assertTrue("Return 1 item", result.size() == 1);
        assertTrue("1st result is San Jose", result.get(0).equals("San Jose"));
    }

    // test match ignore case
    @Test
    public void testAutoCompleteMatchedIgnoreCase() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        List result = trie.autocomplete("san j", 10);
        assertTrue("Return 1 item", result.size() == 1);
        assertTrue("1st result is San Jose", result.get(0).equals("San Jose"));
    }

    // test match deduplication
    @Test
    public void testDeduplication() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        trie.insert("San Diego");
        trie.insert("San Diego");
        List result = trie.autocomplete("san", 10);
        assertTrue("Return 2 item", result.size() == 2);
        assertTrue("1st result is San Diego", result.get(0).equals("San Diego"));
    }

    // test not matched
    @Test
    public void testAutoCompleteNoMatched() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        List result = trie.autocomplete("", 10);
        assertTrue("Return 0 item", result.size() == 0);
    }

    // test autocomplete limit
    @Test
    public void testAutoCompleteLimit() {
        Trie trie = new Trie(); 
        trie.insert("San Diego");
        trie.insert("San Jose");
        trie.insert("California");
        trie.insert("San Dimas");
        trie.insert("San Francisco");
        List result = trie.autocomplete("San", 2);
        assertTrue("Return empty list", result.size() == 2);
        assertTrue("1st result is San Diego", result.get(0).equals("San Diego"));
        assertTrue("2nd result is San Dimas", result.get(1).equals("San Dimas"));

        List result2 = trie.autocomplete("San", 4);
        assertTrue("Return empty list", result2.size() == 4);
        assertTrue("1st result is San Diego", result2.get(0).equals("San Diego"));
        assertTrue("2nd result is San Dimas", result2.get(1).equals("San Dimas"));
        assertTrue("3rd result is San Jose", result2.get(2).equals("San Jose"));
        assertTrue("4th result is San Francisco", result2.get(3).equals("San Francisco"));
    }
}