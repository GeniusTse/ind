package com.indeed.suggest.prefixTree.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(TestTrie.class);
		
      for (Failure failure : result.getFailures()) {
         System.out.println("* "+ failure.toString());
      }
    
      System.out.println("\n\n===============================");
      System.out.println("| All test cases passed: "+ result.wasSuccessful() +" |");
      System.out.println("===============================");
   }
}