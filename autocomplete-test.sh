#! /bin/bash

ant clean compile

HEAP_SIZE=512m
LIB_JARS=`find -H lib -name "*.jar" | tr "\n" ":"`

java -cp $LIB_JARS:build/classes com.indeed.suggest.prefixTree.test.TestRunner
