Explain your approach to the problem
==================================== 
First idea come up in my mind is using something like ElasticSearch or Apache Lucene but I understood it is not the objective of this test.
Second thought was using something link B-Tree, DBMS indexing mechanism but which is not tailormade enough for autocomplete
Of cause, Binary Search (sort input, find first and last postion with prefix) is another option but which less suitable for this problem. 

Finally, I come up a modified Trie.
- assume the autocomplete start match from head
- use prefix tree (trie) to stort location
    - break input location into character level
    - store them as a tree without branch first
    - create a branch when there is a similar location.
    - Example: 
        - input 1: San Diego   
            - S -> a -> n -> -> D -> "i" -> e -> g -> o
        - input 2: San Dimas
            - S -> a -> n -> -> D -> "i" -> m -> a -> s
        - The algorithm is going create a new branch (node children) in "i" 
        - When user type "San Di", the algorithm will find the S -> a -> n -> -> D -> i and list posible answer.
        
- use LinkedList to store node children so that to maintain the sequence.

Test
======================================================
> ./autocomplete-test.sh

What improvements would you make if you had more time?
======================================================
- take more time to study JUNIT since I haven't wrote java in production about 7 years
- add more unit tests 
- add integation test with query.log.  Try to use l as query and ensure autocomplete include (but not only) "match"
- cache most recent result
- in real word, we should delay user input before calling the autocomplete to reduce server call. 

Explain the space and time complexity of your solution.
=======================================================
Build index
- O(number of characters) => O(n)
- or O(no of location * average location name)

Lookup and suggest
- O(tree depth + possible children) => O(logn + p)

Space 
- O(number of characters) in average => O(n)
- O(2 * number of characters) in worst case since there is a duplication map